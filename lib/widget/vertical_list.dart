import 'package:flutter/material.dart';
//import 'package:movie_app/model/movie.dart';

class VerticalList extends StatelessWidget {
//  final int index;
//  VerticalList(this.index);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          elevation: 5,
          child: Row(
            children: <Widget>[
              Container(
                height: 150,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5),
                    topLeft: Radius.circular(5),
                  ),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSxj0M5vbvyeQ8kxcUoRdHMyIjtsQ410YILzsggthfw7GRryQXP',
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                     'Jumenji',
                      style: TextStyle(fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        width: 240,
                        child: Text('Jumanji: The Next Level Distributor Sony Pictures Releasing of Spain Release date December 13, 2019 (2h 04min) Directed by Jake Kasdan Cast Dwayne Johnson, Jack Black, Kevin Hart...')),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10,),
      ],
    );
  }
}

//movieList[index].title,
//movieList[index].imageUrl
