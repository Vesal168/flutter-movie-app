import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './main_drawer.dart';

class DetailScreen extends StatelessWidget {
  static const routeName = '/detail-screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
        drawer: MainDrawer(),
        body: Center(
//          padding: const EdgeInsets.all(30.0),
           child: Column(
             children: <Widget>[
               SizedBox(height: 20,),
//                search button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32,),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.all(Radius.circular(30.0,)),
                    child: TextField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 14.0,),
                        suffix: Material(
                          elevation: 2.0,
                          borderRadius: BorderRadius.all(Radius.circular(30.0,)),
                          child: Icon(Icons.search,),
                        ),
                      ),
                    ),
                  ),
                ),
//                card box
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Row(
                    children: <Widget>[
                    Card(
                      elevation: 10,
                        child: Container(
                          height: 160,
                          width: 160,
                          color: Colors.amber,
                          margin: EdgeInsets.only(right: 1, left: 1),
                        ),
                      ),
                      Card(
                        elevation: 10,
                        child: Container(
                          height: 160,
                          width: 160,
                          color: Colors.amber,
                        ),
                      ),
                    ],
                  ),
                ),
               SizedBox(height: 0.0,),
               Padding(
                 padding: const EdgeInsets.all(30.0),
                 child: Row(
                   children: <Widget>[
                     Card(
                       elevation: 10,
                       child: Container(
                         height: 160,
                         width: 160,
                         color: Colors.amber,
                         margin: EdgeInsets.only(right: 1, left: 1),
                       ),
                     ),
//                      SizedBox(height: 20,),
                     Card(
                       elevation: 10,
                       child: Container(
                         height: 160,
                         width: 160,
                         color: Colors.amber,
                       ),
                     ),
                   ],
                 ),
               ),
             ],
           ),
        ),
    );
  }
}
