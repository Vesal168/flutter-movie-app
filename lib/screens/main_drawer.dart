

import 'package:flutter/material.dart';
import 'package:movie_app/screens/detail_screen.dart';
import 'package:movie_app/screens/movie_home_page.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Theme.of(context).primaryColor,
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(
                      top: 30,
                      bottom: 10,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://scontent.fpnh12-1.fna.fbcdn.net/v/t1.0-9/78385729_550220932206102_8710340349421158400_n.jpg?_nc_cat=103&_nc_ohc=4KdN6HLUFIYAX8VWlX_&_nc_ht=scontent.fpnh12-1.fna&oh=7e6e8209289bf88002701782c31bff9b&oe=5EADD666',
                          ),
                        fit: BoxFit.fill
                      ),
                    ),
                  ),
                  Text(
                      'Khean Visal',
                    style: TextStyle(
                      fontSize: 22,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    'visal@007@gmail.com',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.person, color: Colors.red, size: 30,),
            title: Text('Profile',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: (){
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(DetailScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.movie,color: Colors.red, size: 30,),
            title: Text('Movie',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: (){
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(MovieHomePage.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.credit_card, color: Colors.red, size: 30,),
            title: Text('Activation Card',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
          ListTile(
            leading: Icon(Icons.theaters, color: Colors.red, size: 30,),
            title: Text('Cinemas',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
          ListTile(
            leading: Icon(Icons.history, color: Colors.red, size: 30),
            title: Text('History',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
          ListTile(
            leading: Icon(Icons.people, color: Colors.red, size: 30,),
            title: Text('About Us',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
          ListTile(
            leading: Icon(Icons.settings, color: Colors.red, size: 30,),
            title: Text('Setting',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
          ListTile(
            leading: Icon(Icons.arrow_back, color: Colors.red, size: 30,),
            title: Text('Logout',
              style: TextStyle(fontSize: 18,),
            ),
            onTap: null,
          ),
        ],
      ),
    );
  }
}
