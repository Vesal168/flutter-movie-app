import 'package:flutter/material.dart';
import './main_drawer.dart';
import './detail_screen.dart';

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('We are at HomeScreen now.',
              style: TextStyle(
                fontSize: 22,
              ),
            ),
            RaisedButton(
              child: Text('Go to detail'),
              onPressed: (){
//                Navigator.push(context, MaterialPageRoute(
//                    builder: (context) => DetailScreen(),
//                ));
                  Navigator.of(context).pushNamed(DetailScreen.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }
}
