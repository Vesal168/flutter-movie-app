import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../widget/vertical_list.dart';
import '../widget/horizontal_list.dart';
import './main_drawer.dart';

class MovieHomePage extends StatelessWidget {
  static const routeName = '/movie-home-page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Page', textAlign: TextAlign.center),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          ),
        ],
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Recomment',
                    style: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  FlatButton(
                    child: Text('View All'),
                    onPressed: (){},
                  ),
                ],
              ),
            ),
            Container(
              height: 280,
              child: ListView(
                scrollDirection: Axis.horizontal,
//                itemCount: movieList.length,
//                itemBuilder: (ctx, i) => HorizontalList(i),
                children: <Widget>[
                  HorizontalList(),
                  HorizontalList(),
                  HorizontalList(),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Best of 2020',
                    style: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  FlatButton(
                    child: Text('View All'),
                    onPressed: (){},
                  ),
                ],
              ),
            ),
            Container(
              height: 500,
              padding: const EdgeInsets.symmetric(horizontal: 10,),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
//                itemCount: movieList.length,
//                itemBuilder: (ctx, i) => ,
                children: <Widget>[
                  VerticalList(),
                  VerticalList(),
                  VerticalList(),
                  VerticalList(),
                  VerticalList(),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Top Rated Movies',
                    style: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  FlatButton(
                    child: Text('View All'),
                    onPressed: (){},
                  ),
                ],
              ),
            ),
            Container(
              height: 280,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  HorizontalList(),
                  HorizontalList(),
                  HorizontalList(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
