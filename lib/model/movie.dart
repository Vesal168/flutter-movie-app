class Movie{
  final String id;
  final String title;
  final String imageUrl;
  final String description;
  final String rating;
  final String year;
  final String duration;


  Movie({
    this.id,
    this.title,
    this.imageUrl,
    this.description,
    this.rating,
    this.year,
    this.duration,
  });
}
final movieList = [
  Movie(
    id: '012012',
    title: 'Jumanji next level',
    imageUrl: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSxj0M5vbvyeQ8kxcUoRdHMyIjtsQ410YILzsggthfw7GRryQXP',
    description: 'Jumanji: The Next Level Distributor Sony Pictures Releasing of Spain Release date December 13, 2019 (2h 04min) Directed by Jake Kasdan Cast Dwayne Johnson, Jack Black, Kevin Hart...',
    rating: '9.9',
    year: '2020',
    duration: '140 min'
  ),
  Movie(
      id: '223021',
      title: 'Ip Man 4 ',
      imageUrl: 'https://www.wellgousa.com/films/ip-man-4-the-finale',
      description: 'With less than a week to go until its Christmas Day release, a new U.S. trailer has arrived online for Ip Man 4 which sees Donnie Yen reprising his role as the legendary Wing Chun grandmaster...',
      rating: '11.9',
      year: '2020',
      duration: '140 min'
  ),
  Movie(
      id: '094435',
      title: 'The Rise of Skywalker',
      imageUrl: 'https://www.boxofficepro.com/weekend-estimates-analysis-star-wars-rise-of-skywalker/',
      description: 'Rise of Skywalker is now trending for a 175.5 million domestic debut, slightly down from Saturday’s optimistic projections but still on target with final pre-release forecasts....',
      rating: '10.9',
      year: '2020',
      duration: '170 min'
  ),

];